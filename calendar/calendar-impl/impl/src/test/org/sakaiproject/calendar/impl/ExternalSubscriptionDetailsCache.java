package org.sakaiproject.calendar.impl;

import org.sakaiproject.calendar.api.ExternalSubscriptionDetails;
import org.sakaiproject.memory.api.*;

import java.util.*;

public class ExternalSubscriptionDetailsCache<K extends String, V extends ExternalSubscriptionDetails>
       implements Cache<K, V> {

    private Map<K, V> map = new TreeMap<>();

    @Override
    public V get(K key) {
        return map.get(key);
    }

    @Override
    public Map<K, V> getAll(Set<? extends K> keys) {
        return map;
    }

    @Override
    public boolean containsKey(K key) {
        return map.containsKey(key);
    }

    @Override
    public void put(K key, V payload) {
        map.put(key, payload);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        putAll(m);
    }

    @Override
    public boolean remove(K key) {
        return remove(key);
    }

    @Override
    public void removeAll(Set<? extends K> keys) {
        removeAll(keys);
    }

    @Override
    public void removeAll() {
        clear();
    }

    @Override
    public void clear() {
        clear();
    }

    @Override
    public Configuration getConfiguration() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void close() {

    }

    @Override
    public <T> T unwrap(Class<T> clazz) {
        return null;
    }

    @Override
    public void registerCacheEventListener(CacheEventListener cacheEventListener) {

    }

    @Override
    public CacheStatistics getCacheStatistics() {
        return null;
    }

    @Override
    public Properties getProperties(boolean includeExpensiveDetails) {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void attachLoader(CacheLoader cacheLoader) {

    }

    @Override
    public boolean isDistributed() {
        return false;
    }

}
