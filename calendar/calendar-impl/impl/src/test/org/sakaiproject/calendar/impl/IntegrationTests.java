/**
 * Copyright (c) 2003-2017 The Apereo Foundation
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *             http://opensource.org/licenses/ecl2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sakaiproject.calendar.impl;

import net.sf.ehcache.CacheManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.sakaiproject.calendar.api.ExternalCalendarSubscriptionService;
import org.sakaiproject.memory.api.Cache;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(locations={"/calendar-caches.xml", "/test-resources.xml"})
public class IntegrationTests extends AbstractJUnit4SpringContextTests {

	private SubscriptionCache subscriptionCache;

	@Before
	public void onSetUp() {
		CacheManager cacheManager = (CacheManager) applicationContext.getBean(
		 		"org.sakaiproject.memory.api.MemoryService.cacheManager");
		Clock clock =  Clock.fixed(Instant.now(), ZoneId.systemDefault());

		Cache<String, BaseExternalSubscriptionDetails> cache = new ExternalSubscriptionDetailsCache<>();

		subscriptionCache = new SubscriptionCache(cache, clock);
		/*cache = (SubscriptionCache) cacheManager.getCache(
				"org.sakaiproject.calendar.impl.BaseExternalCacheSubscriptionService.institutionalCache");*/
	}

	@Test
	public void testNothing() {
		Assert.assertNull(subscriptionCache.get("not-in-cache"));
	}

	@Test
	public void testCacheRoundtrip() throws InterruptedException {
		BaseExternalCalendarSubscriptionService subscription = new BaseExternalCalendarSubscriptionService();
		subscription.loadCalendarSubscriptionFromUrl("http://example.com",
				ExternalCalendarSubscriptionService.SAK_PROP_EXTSUBSCRIPTIONS_URL /* context */, "xxxx", "yyyy");

		// String url,  String context, String userId, String tzid, String calendarName, String forcedEventType)
		//  * Perform an import given the import type. (SAK-33451)
		//	 * @param importType Type such as Outlook, MeetingMaker, etc. defined in the CalendarImporterService interface.
		assertEquals(subscription,  subscriptionCache.get("http://example.com"));
	}

}
