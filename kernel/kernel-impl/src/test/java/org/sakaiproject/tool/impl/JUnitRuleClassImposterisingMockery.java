package org.sakaiproject.tool.impl;

import org.jmock.integration.junit4.JUnitRuleMockery;
import org.jmock.lib.concurrent.Synchroniser;
import org.jmock.lib.legacy.ClassImposteriser;

/**
 * from https://github.com/jmock-developers/jmock-library/issues/36
 */
public class JUnitRuleClassImposterisingMockery extends JUnitRuleMockery {

    private final Synchroniser synchroniser = new Synchroniser();
    {
        setImposteriser(ClassImposteriser.INSTANCE);
        setThreadingPolicy(synchroniser); // otherwise we get errors on the finalizer thread
    }
    public Synchroniser getSynchroniser() {
        return synchroniser;
    }
}
